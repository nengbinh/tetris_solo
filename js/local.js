class Local {
    constructor() {
        // game object
        this.game = new Game();
        this.INTERVAL = 200
        // time
        this.timeCount = 0
        this.time = 0
    }
    // bind key
    bindKeyEvent() {
        document.onkeydown = e =>{
            switch(e.keyCode){
                case 38: // up
                    this.game.keyMove('up')
                break
                case 39: // right
                    this.game.keyMove('right')
                break
                case 40: // down
                    this.game.keyMove('down')
                break
                case 37: // left
                    this.game.keyMove('left')
                break
                case 32: // space
                    this.game.fall()
                break
            }
        }
    }
    //
    start() {
        // get element obj on html
        let doms = {
            gameDiv: document.getElementById('game'),
            nextDiv: document.getElementById('next'),
            timeDiv: document.getElementById('time'),
            scoreDiv: document.getElementById('score'),
            resultDiv: document.getElementById('result')
        }
        // init all data
        this.game.init(doms, this.randomType(), this.randomDir() )
        this.bindKeyEvent() // bind keypress to game
        this.game.createNext(this.randomType(), this.randomDir()) // prepare for next block
        // timer for block go down slowly
        this.timer = setInterval(() => {
            this.move()
        }, this.INTERVAL);
    };
    // block go down slowly
    move() {
        this.timeProcess()
        if(!this.game.keyMove('down')){
            this.game.stay() // change it style
            let line = this.game.checkFinish() // check if any lines that done
            if(line) this.game.addScore(line) // add score if clear any line
            let gameOver = this.game.checkGameOver() //
            if(gameOver){
                this.game.gameover(false)
                this.stop()
            }else{
                this.game.createNext(this.randomType(), this.randomDir())  // prepare for next block
            }
        }
    }
    // time
    timeProcess() {
        this.timeCount += 1
        if(this.timeCount == 5){
            this.timeCount = 0
            this.time += 1
            this.game.setTime(this.time)
        }
    }
    // get random block type
    randomType() {
        return Math.ceil(Math.random() * 7)
    }
    // get random block direction
    randomDir() {
        return Math.ceil(Math.random() * 4)
    }
    // gameover 
    stop() {
        if(this.timer) {
            clearInterval(this.timer)
            this.timer = null
        }
        document.onkeydown = null
    }
}