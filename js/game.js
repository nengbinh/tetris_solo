class Game {

    constructor() {
        // dom element
        this.gameDiv
        this.nextDiv
        this.timeDiv
        this.scoreDiv
        this.resultDiv
        // game score
        this.scorePoint = 0
        // game array
        this.gameData = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ]
        // now block & next block
        this.cur
        this.next
        // use to save div
        this.nextDivs = []
        this.gameDivs = []
    }
    // fill up the container with all divs
    initDiv (container, data, divs){
        for(let i in data){
            let div = []
            for(let j in data[0]){
                // html elements
                let newNode = document.createElement('div')
                newNode.className = 'none'
                newNode.style.top = (i*20) + 'px'
                newNode.style.left = (j*20) + 'px'
                container.appendChild(newNode)
                div.push(newNode)
            }
            divs.push(div)
        }
    }
    // change divs style with game data array
    refreshDiv (data, div){
        for(let i in data){
            for(let j in data[0]){
                if(data[i][j] == 0){
                    div[i][j].className = 'none';
                }else if(data[i][j] == 1){
                    div[i][j].className = 'done';
                }else if(data[i][j] == 2){
                    div[i][j].className = 'current';
                }
            }
        }
    }
    // set game data array & clear game data
    setData(action=false) {
        for(let i in this.cur.data){
            i = parseInt(i)
            for(let j in this.cur.data[0]){
                j = parseInt(j)
                // check position if its valid then set game data
                if(this.checkP(this.cur.origin, i, j)) {
                    if(action){
                        this.gameData[this.cur.origin.x + i][this.cur.origin.y + j] = 0
                    }else{
                        this.gameData[this.cur.origin.x + i][this.cur.origin.y + j] = this.cur.data[i][j]
                    }
                }
            }
        }
    }
    // check position if valid
    checkP(pos, x, y){
        if(pos.x + x <0) return false // too left
        if(pos.x + x >= this.gameData.length) return false // too down
        if(pos.y + y <0) return false // too top
        if(pos.y + y >= this.gameData[0].length) return false // too right
        if(this.gameData[pos.x + x][pos.y + y] == 1) return false // that position have some done block
        return true
    }
    // check data if valid
    checkData(pos, data) {
        for(let i=0; i<data.length; i++){
            for(let j=0; j<data[0].length; j++){
                if(data[i][j] !=0){
                    if(!this.checkP(pos, i, j)) return false;
                }
            }
        }
        return true
    }
    // bind keypress move
    keyMove(direction) {
        if(this.cur.blockTest(this, direction)){
            this.setData(true) // clear data array
            this.cur.blockMove(direction) // change position
            this.setData() // update data array
            this.refreshDiv(this.gameData, this.gameDivs) // update html div class
            return true
        }
        return false
    }
    // when block gets to bottom make it stay
    stay() {
        for(let i in this.cur.data){
            i = parseInt(i)
            for(let j in this.cur.data[0]){
                j = parseInt(j)
                if(this.checkP(this.cur.origin, i, j)){
                    if(this.gameData[this.cur.origin.x + i][this.cur.origin.y + j] == 2){
                        this.gameData[this.cur.origin.x + i][this.cur.origin.y + j] = 1
                    }
                }
            }
        }
        this.refreshDiv(this.gameData, this.gameDivs)
    }
    // display game result
    gameover(win) {
        if(win){
            this.resultDiv.innerHTML = 'You Win'
        }else{
            this.resultDiv.innerHTML = 'You Lose'
        }
    }
    // use next block
    createNext(type, dir) {
        this.cur = this.next // next block data pass to current
        this.setData() // add it in to data
        this.next = SquareFactory(type, dir) // create next block
        this.refreshDiv(this.gameData, this.gameDivs) // when everything ready , change style
        this.refreshDiv(this.next.data, this.nextDivs)
    }
    // block fall down
    fall() {
        while(this.keyMove('down')){}
    }
    // set game time
    setTime(time) {
        this.timeDiv.innerHTML = time
    }
    addScore(line) {
        let s = 0
        switch(line){
            case 1:
                s = 10
            break
            case 2:
                s = 30
            break
            case 3:
                s = 60
            break
            case 4:
                s = 100
            break
        }
        this.scorePoint += s
        this.scoreDiv.innerHTML = this.scorePoint
    }
    // check if any lines that finish
    checkFinish() {
        let line = 0
        for(var i=this.gameData.length-1; i>=0; i--){
            let clear = true
            for(let j=0; j<this.gameData[0].length; j++){
                if(this.gameData[i][j] != 1){ // if there has anything that is not done then break
                    clear = false
                    break
                }
            }
            if(clear){ // if line could be clear now
                line += 1
                // move everyline down one
                for(let m=i; m>0; m--){
                    for(let n=0; n<this.gameData[0].length; n++){
                        this.gameData[m][n] = this.gameData[m-1][n]
                    }
                }
                // top line fill up with empty
                for(let n=0; n<this.gameData[0].length; n++){
                    this.gameData[0][n] = 0
                }
                i++
            }
        }
        return line
    }
    // to check if game over
    checkGameOver() {
        let gameOver = false;
        for(let i in this.gameData[0]){
            // check last line if is 1 , 1 mean it stay, then is end up gameover
            if(this.gameData[0][i] == 1){
                gameOver = true
            }
        }
        return gameOver
    }
    // 
    init (doms, type, dir){
        this.gameDiv = doms.gameDiv // doms from local.js
        this.nextDiv = doms.nextDiv
        this.timeDiv = doms.timeDiv
        this.scoreDiv = doms.scoreDiv
        this.resultDiv = doms.resultDiv
        this.next = SquareFactory(type,dir) // create it with 4 * 4 block
        this.initDiv(this.gameDiv, this.gameData, this.gameDivs) // fill up html element
        this.initDiv(this.nextDiv, this.next.data, this.nextDivs)
        this.refreshDiv(this.next.data, this.nextDivs) // change element style
    }
}