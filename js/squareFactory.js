class Block1 extends Square{
    constructor(){
        super()
        // block rotate
        this.rotates = [
            [
                [0, 2, 0, 0],
                [0, 2, 0, 0],
                [0, 2, 0, 0],
                [0, 2, 0, 0],
            ],
            [
                [0, 0, 0, 0],
                [2, 2, 2, 2],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [0, 2, 0, 0],
                [0, 2, 0, 0],
                [0, 2, 0, 0],
                [0, 2, 0, 0],
            ],
            [
                [0, 0, 0, 0],
                [2, 2, 2, 2],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        ]
    }
}

class Block2 extends Square{
    constructor(){
        super()
        // block rotate
        this.rotates = [
            [
                [0, 2, 0, 0],
                [2, 2, 2, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [0, 2, 0, 0],
                [0, 2, 2, 0],
                [0, 2, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [0, 0, 0, 0],
                [2, 2, 2, 0],
                [0, 2, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [0, 2, 0, 0],
                [2, 2, 0, 0],
                [0, 2, 0, 0],
                [0, 0, 0, 0],
            ]
        ]
    }
}

class Block3 extends Square{
    constructor(){
        super()
        // block rotate
        this.rotates = [
            [
                [2, 2, 2, 0],
                [0, 0, 2, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [0, 2, 0, 0],
                [0, 2, 0, 0],
                [2, 2, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 0, 0, 0],
                [2, 2, 2, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 2, 0, 0],
                [2, 0, 0, 0],
                [2, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        ]
    }
}

class Block4 extends Square{
    constructor(){
        super()
        // block rotate
        this.rotates = [
            [
                [2, 2, 2, 0],
                [2, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 2, 0, 0],
                [0, 2, 0, 0],
                [0, 2, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [0, 0, 2, 0],
                [2, 2, 2, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 0, 0, 0],
                [2, 0, 0, 0],
                [2, 2, 0, 0],
                [0, 0, 0, 0],
            ]
        ]
    }
}

class Block5 extends Square{
    constructor(){
        super()
        // block rotate
        this.rotates = [
            [
                [2, 2, 0, 0],
                [2, 2, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 2, 0, 0],
                [2, 2, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 2, 0, 0],
                [2, 2, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 2, 0, 0],
                [2, 2, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        ]
    }
}

class Block6 extends Square{
    constructor(){
        super()
        // block rotate
        this.rotates = [
            [
                [0, 2, 2, 0],
                [2, 2, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 0, 0, 0],
                [2, 2, 0, 0],
                [0, 2, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [0, 2, 2, 0],
                [2, 2, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 0, 0, 0],
                [2, 2, 0, 0],
                [0, 2, 0, 0],
                [0, 0, 0, 0],
            ]
        ]
    }
}

class Block7 extends Square{
    constructor(){
        super()
        // block rotate
        this.rotates = [
            [
                [2, 2, 0, 0],
                [0, 2, 2, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [0, 2, 0, 0],
                [2, 2, 0, 0],
                [2, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [2, 2, 0, 0],
                [0, 2, 2, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [0, 2, 0, 0],
                [2, 2, 0, 0],
                [2, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        ]
    }
}

// factory
function SquareFactory(index, dir) {
    let block;
    switch(index){
        case 1:
            block = new Block1()
        break
        case 2:
            block = new Block2()
        break
        case 3:
            block = new Block3()
        break
        case 4:
            block = new Block4()
        break
        case 5:
            block = new Block5()
        break
        case 6:
            block = new Block6()
        break
        case 7:
            block = new Block7()
        break
    }
    // set init position
    block.origin.x = 0
    block.origin.y = 3
    block.blockMove('up', dir)
    return block
}
